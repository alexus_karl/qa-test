# QA-Test



## Test Description
This is a test for BalboDigital as per recruitment process


## Tasks

Compile a list of several testable user flows, cases, or scenarios. This doesn't have to cover every possible real-life case.

| Site        | URL                                |
| ----------- | ---------------------------------- |
| Online Shop | https://qa-challenge.codesubmit.io |

Make sure to test scenarios for all provided user accounts.

| User                    | Description                                                             |
| ----------------------- | ----------------------------------------------------------------------- |
| standard_user           | The site should work as expected for this user                          |
| locked_out_user         | User is locked out and should not be able to log in.                    |
| problem_user            | Images are not loading for this user.                                   |
| performance_glitch_user | This user has high loading times. Does the site still work as expected? |

- Implement automated browser tests for all flows. Use any testing technology you'd like – Cypress, Selenium, or any other you think would work well

## Tools

This project will be constructed using Python Selenium-Robot Framework

- [ ] Python
- [ ] Selenium-Robot Framework


