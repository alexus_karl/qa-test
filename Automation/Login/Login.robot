*** Settings ***
Library     SeleniumLibrary
Library   SeleniumLibrary

*** Variables ***
${BROWSER}                  chrome
${URL}                      https://qa-challenge.codesubmit.io
${STANDARD_USER}            standard_user
${LOCKED_OUT_USER}          locked_out_user
${PROBLEM_USER}             problem_user
${PERFORMANCE_GLITCH_USER}  performance_glitch_user
${PASSWORD}                 secret_sauce
${INVALID_PASSWORD}         secretsauce
${USERNAME_FIELD}           id:user-name
${PASSWORD_FIELD}           id:password
${LOGIN_BTN}                id:login-button
${ERROR_XPATH}              xpath:/html/body/div/div/div[2]/div[1]/div/div/form/div[3]/h3
${PAGE_HEADER}              xpath:/html/body/div/div/div/div[1]/div[1]/div[2]/div
${LOGOUT_BTN}               xpath:/html/body/div/div/div/div[1]/div[1]/div[1]/div/div[2]/div[1]/nav/a[3]
${MENU_BTN}                 xpath:/html/body/div/div/div/div[1]/div[1]/div[1]/div/div[1]/div/button
${LOGIN_HEADER}             xpath:xpath:/html/body/div/div/div[1]
${WRONG_IMAGE}              https://qa-challenge.codesubmit.io/static/media/sl-404.168b1cce.jpg
${RIGHT_IMAGE1}             https://qa-challenge.codesubmit.io/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg
${RIGHT_IMAGE2}             https://qa-challenge.codesubmit.io/static/media/bike-light-1200x1500.37c843b0.jpg

*** Test Cases ***
TC_001
    [Documentation]  Used to login to the page with blank username and password
    Open Browser     ${URL}         ${BROWSER}
    MAXIMIZE BROWSER WINDOW
    CLICK BUTTON     ${LOGIN_BTN}
    ELEMENT TEXT SHOULD BE      ${ERROR_XPATH}      Epic sadface: Username is required
    SLEEP   2s

TC_002
    [Documentation]  Used to login to the page with valid username and blank password
    GO TO            ${URL}
    INPUT TEXT       ${USERNAME_FIELD}   ${STANDARD_USER}
    CLICK BUTTON     ${LOGIN_BTN}
    ELEMENT TEXT SHOULD BE      ${ERROR_XPATH}      Epic sadface: Password is required
    CLEAR ELEMENT TEXT   ${USERNAME_FIELD}
    CLEAR ELEMENT TEXT   ${PASSWORD_FIELD}
    SLEEP   2s

TC_003
    [Documentation]  Used to login to the page with blank username and a valid password
    GO TO            ${URL}
    INPUT TEXT       ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK BUTTON     ${LOGIN_BTN}
    ELEMENT TEXT SHOULD BE      ${ERROR_XPATH}      Epic sadface: Username is required
    SLEEP    2s

TC_004
    [Documentation]  Used to login to the page with valid user and invalid password
    GO TO            ${URL}
    INPUT TEXT       ${USERNAME_FIELD}   ${STANDARD_USER}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${INVALID_PASSWORD}
    CLICK BUTTON     ${LOGIN_BTN}
    ELEMENT TEXT SHOULD BE   ${ERROR_XPATH}      Epic sadface: Username and password do not match any user in this service
    SLEEP    3s

TC_005
    [Documentation]  Used to login page with locked out user
    GO TO            ${URL}
    INPUT TEXT       ${USERNAME_FIELD}   ${LOCKED_OUT_USER}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK BUTTON     ${LOGIN_BTN}
    ELEMENT TEXT SHOULD BE   ${ERROR_XPATH}      Epic sadface: Sorry, this user has been locked out.

TC_006
    [Documentation]  Used to login page with locked out user
    GO TO            ${URL}
    INPUT TEXT       ${USERNAME_FIELD}   ${LOCKED_OUT_USER}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK BUTTON     ${LOGIN_BTN}
    ELEMENT TEXT SHOULD BE   ${ERROR_XPATH}      Epic sadface: Sorry, this user has been locked out.

TC_007
    [Documentation]  Used to login page with performance glitch user
    GO TO            ${URL}
    INPUT TEXT       ${USERNAME_FIELD}   ${PERFORMANCE_GLITCH_USER}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK BUTTON     ${LOGIN_BTN}
    Set Selenium Implicit Wait   1s
    ELEMENT SHOULD BE VISIBLE    ${PAGE_HEADER}
    SLEEP   2s

TC_008
    [Documentation]  Check if the user can logout
    CLICK ELEMENT   ${MENU_BTN}
    SLEEP    1s
    CLICK ELEMENT   ${LOGOUT_BTN}
    SLEEP   1s
    ELEMENT SHOULD BE VISIBLE    ${USERNAME_FIELD}

TC_009
    [Documentation]  Check if the problem user loads the wrong images
    GO TO            ${URL}
    INPUT TEXT       ${USERNAME_FIELD}   ${PROBLEM_USER}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK BUTTON     ${LOGIN_BTN}
    PAGE SHOULD NOT CONTAIN IMAGE    /static/media/sauce-backpack-1200x1500.0a0b85a3.jpg
    CLICK ELEMENT   ${MENU_BTN}
    SLEEP    1s
    CLICK ELEMENT   ${LOGOUT_BTN}
    SLEEP   1S

TC_010
    [Documentation]  Used to login page with standard user
    GO TO            ${URL}
    INPUT TEXT       ${USERNAME_FIELD}   ${STANDARD_USER}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK BUTTON     ${LOGIN_BTN}
    ELEMENT SHOULD BE VISIBLE    ${PAGE_HEADER}
    PAGE SHOULD NOT CONTAIN IMAGE    /static/media/sl-404.168b1cce.jpg
    SLEEP   2s
